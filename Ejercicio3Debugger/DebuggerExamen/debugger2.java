package DebuggerExamen;

import java.util.Scanner;

public class debugger2 {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);

		System.out.println("Introduce un mes escrito");
		String mes = lector.nextLine();

		mes = mes.toLowerCase();

		switch (mes) {
		case "enero":
		case "marzo":
		case "mayo":
		case "julio":
		case "agosto":
		case "octubre":
		case "diciembre":
			System.out.println("Tiene 31 dias");
			break;

		case "febrero":
			System.out.println("Tiene 28 dias");
			break;

		case "abril":
		case "junio":
		case "septiembre":
		case "noviembre":
			System.out.println("Tiene 30 dias");
			break;

		default:
			System.out.println("Opcion no contemplada");
		}

		lector.close();
	}

}
